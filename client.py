#!/usr/bin/env python
import socket
import sys
import os

class TCPclient():
    def __init__(self,ip,port):
        """
        This function is for initialisation
        """
        self.ip=ip
        self.port=port
        self.sock=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_address=(self.ip,self.port)
        self.sock.connect(self.server_address)
    
    def work(self):
        """
        This function return answers of serverside and print it
        """
        try:
            while True:
                data=self.sock.recv(8192)
                if data == "":
                    exit()
                print(data)
                req = raw_input()
                self.sock.sendall(req)
        except socket.timeout:
            self.sock.close()




"""
This is main where we create object of TCPclient class
"""
if __name__ == '__main__':
    print("Enter IP address ")
    ip=raw_input()
    print("Enter port number")
    port=raw_input()
    client = TCPclient(ip,port)
    client.work()
